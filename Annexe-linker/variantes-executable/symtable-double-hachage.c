#include <stdlib.h>
#include <string.h>
#include "symtable.h"
#include "error.h"

/**
 * @file
 * @brief Implantation des tables des symboles des exécutables
 * @author F. Boulier
 * @date novembre 2010
 */

/**
 * @brief Constructeur. Initialise \p table. Cette fonction devrait
 * être appelée avant toute autre utilisation de \p table.
 * @param[out] table un exécutable
 */

void init_symtable (struct symtable* table)
{
    init_table_double_hachage (&table->T, 400, (fonction_double_hachage*)0);
    init_stats (&table->mesures);
}

/**
 * @brief Destructeur. Libère les ressources consommées par \p table.
 * Cette fonction devrait être appelée après la dernière utilisation
 * de \p table.
 * @param[in,out] table un exécutable
 */


void clear_symtable (struct symtable* table)
{
    clear_table_double_hachage (&table->T);
    clear_stats (&table->mesures);
}

/**
 * @brief Enregistre le symbole \p sym dans la table des symboles
 * de l'exécutable \p table. Le symbole ne doit pas déjà appartenir à
 * la table. Supposons \p sym global. Si un
 * symbole de même nom, indéfini, est déjà présent, ce
 * symbole devient défini. Si un symbole de même nom, défini,
 * est déjà présent, on a affaire à une définition multiple.
 * @param[in,out] table un exécutable
 * @param[in] sym un symbole
 */

void enregistrer_dans_symtable (struct symtable* table, struct symbole* sym)
{   struct symbole* symp;
    int nbcomp;

/* On incrémente le compteur d'appels à enregistrer et rechercher */
    incrementer_nb_acces_dico_stats (&table->mesures, 1);
    nbcomp = rechercher_symbole_dans_table_double_hachage 
						(&symp, sym->ident, &table->T);
    incrementer_nb_comp_chaines_stats (&table->mesures, nbcomp);
    if (symp)
    {   if (est_defini_symbole (sym))
        {   if (est_indefini_symbole (symp))
                changer_type_symbole (symp, sym->type);
            ajouter_definition_symbole (symp);
        }
    } else
    {
/* Le nb de définitions vaut 1 ou 0 suivant que le symbole est défini ou pas */
        if (est_defini_symbole (sym))
            changer_nbdef_symbole (sym, 1);
        else
            changer_nbdef_symbole (sym, 0);
	ajouter_symbole_dans_table_double_hachage (&table->T, sym);
/* On incrémente le compteur de symboles présents dans la table */
        incrementer_nb_symboles_stats (&table->mesures, 1);
    }
    imprimer_stats (&table->mesures);
}

/**
 * @brief Recherche un symbole d'identificateur \p ident 
 * dans la table des symboles de \p table
 * Retourne l'adresse de ce symbole ou le pointeur nul si le symbole
 * est inconnu.
 * @param[in] ident un identificateur de symbole
 * @param[in] table un exécutable
 */

struct symbole* rechercher_dans_symtable (char* clef, struct symtable* table)
{   int nbcomp;
    struct symbole* sym;

/* On incrémente le compteur d'appels à enregistrer et rechercher */
    incrementer_nb_acces_dico_stats (&table->mesures, 1);
    nbcomp = rechercher_symbole_dans_table_double_hachage 
						(&sym, clef, &table->T);
    incrementer_nb_comp_chaines_stats (&table->mesures, nbcomp);
    imprimer_stats (&table->mesures);
    return sym;
}

/*
 * @brief Imprime le résultat de l'édition des liens
 * @param[in] table une table des symboles
 */

int synthese_symtable (struct symtable* table)
{
    struct symbole* sym;
    bool ok, first;
    int i;

    printf ("taux de remplissage = %f\n", 
		taux_double_remplissage_table_double_hachage (&table->T));
    printf ("nombre de collisions = %d\n", table->T.nb_collisions);
    printf ("nombre de collisions théorique = %f\n", table->T.nb_collisions_th);
    printf ("longueur max des sondages = %d\n", table->T.lmax_sondages);

    ok = true;
    first = true;
    for (i = 0; i < table->T.N; i++)
    {	if (table->T.tab [i].etat == alveole_occupe &&
		est_indefini_symbole (&table->T.tab [i].sym))
	{   if (first)
                printf ("symboles indefinis\n");
            printf ("\t%s\n", table->T.tab [i].sym.ident);
            first = false;
            ok = false;
        }
    }
    sym = rechercher_dans_symtable ("main", table);
    if (sym == (struct symbole*)0)
    {	if (first)
	    printf ("symboles indefinis\n");
	printf ("\t%s\n", "main");
	ok = false;
    }
    first = true;
    for (i = 0; i < table->T.N; i++)
    {	if (table->T.tab [i].etat == alveole_occupe &&
		est_duplique_symbole (&table->T.tab [i].sym))
	{   if (first)
                printf ("symboles dupliques\n");
            printf ("\t%s\n", table->T.tab [i].sym.ident);
            first = false;
            ok = false;
        }
    }
    if (ok)
	printf ("edition des liens reussie\n");
    return ok ? 0 : 1;
}

