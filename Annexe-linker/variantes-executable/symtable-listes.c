#include <stdlib.h>
#include <string.h>
#include "symtable.h"
#include "error.h"

/**
 * @file
 * @brief Implantation des tables des symboles des exécutables
 * @author F. Boulier
 * @date novembre 2010
 */

/**
 * @brief Constructeur. Initialise \p table. Cette fonction devrait
 * être appelée avant toute autre utilisation de \p table.
 * @param[out] table une table des symboles
 */

void init_symtable (struct symtable* table)
{
    init_liste_symbole (&table->L);
    init_stats (&table->mesures);
}

/**
 * @brief Destructeur. Libère les ressources consommées par \p table.
 * Cette fonction devrait être appelée après la dernière utilisation
 * de \p table.
 * @param[in,out] table une table des symboles
 */


void clear_symtable (struct symtable* table)
{
    clear_liste_symbole (&table->L);
    clear_stats (&table->mesures);
}

/**
 * @brief Enregistre le symbole \p sym dans \p table. 
 * Le symbole \p sym est supposé global. Si un
 * symbole de même nom, indéfini, est déjà présent, ce
 * symbole devient défini. Si un symbole de même nom, défini,
 * est déjà présent, on a affaire à une définition multiple.
 * @param[in,out] table une table des symboles
 * @param[in] sym un symbole
 */

void enregistrer_dans_symtable (struct symtable* table, struct symbole* sym)
{   struct symbole* elt;
    int nbcomp;

/* On incrémente le compteur d'appels à enregistrer et rechercher */
    incrementer_nb_acces_dico_stats (&table->mesures, 1);
    nbcomp = rechercher_dans_liste_symbole (&elt, sym->ident, &table->L);
    incrementer_nb_comp_chaines_stats (&table->mesures, nbcomp);
    if (elt)
    {	if (est_defini_symbole (sym))
	{   if (est_indefini_symbole (elt))
		changer_type_symbole (elt, sym->type);
	    ajouter_definition_symbole (elt);
	}
    } else
    {   
/* Le nb de définitions vaut 1 ou 0 suivant que le symbole est défini ou pas */
	if (est_defini_symbole (sym))
            changer_nbdef_symbole (sym, 1);
        else
            changer_nbdef_symbole (sym, 0);
	ajouter_en_tete_liste_symbole (&table->L, sym);
/* On incrémente le compteur de symboles présents dans la table */
        incrementer_nb_symboles_stats (&table->mesures, 1);
    } 
    imprimer_stats (&table->mesures);
}

/**
 * @brief Recherche un symbole d'identificateur \p ident 
 * dans la table des symboles de \p table
 * Retourne l'adresse de ce symbole ou le pointeur nul si le symbole
 * est inconnu.
 * @param[in] ident un identificateur de symbole
 * @param[in] table une table des symboles
 */

struct symbole* rechercher_dans_symtable (char* clef, struct symtable* table)
{   struct symbole* elt;
    int nbcomp;

/* On incrémente le compteur d'appels à enregistrer et rechercher */
    incrementer_nb_acces_dico_stats (&table->mesures, 1);
    nbcomp = rechercher_dans_liste_symbole (&elt, clef, &table->L);
    incrementer_nb_comp_chaines_stats (&table->mesures, nbcomp);
    imprimer_stats (&table->mesures);
    return elt;
}

/*
 * @brief Imprime le résultat de l'édition des liens
 * @param[in] table une table des symboles
 */

int synthese_symtable (struct symtable* table)
{   struct maillon_symbole* M;
    struct symbole* sym;
    bool ok, first;

    ok = true;
    first = true;
    for (M = table->L.tete; M; M = M->next)
    {	if (est_indefini_symbole (&M->value))
	{   if (first)
		printf ("symboles indefinis\n");
	    printf ("\t%s\n", M->value.ident);
	    ok = false;
	    first = false;
	}
    }
    sym = rechercher_dans_symtable ("main", table);
    if (sym == (struct symbole*)0)
    {	if (first)
	    printf ("symboles indefinis\n");
	printf ("\t%s\n", "main");
	ok = false;
	first = false;
    }
    first = true;
    for (M = table->L.tete; M; M = M->next)
    {   if (!est_faible_symbole (&M->value) && M->value.nbdef >= 2)
	{   if (first)
                printf ("symboles dupliques\n");
	    printf ("\t%s\n", M->value.ident);
	    ok = false;
	    first = false;
	}
    }
    if (ok)
	printf ("edition des liens reussie\n");
    return ok ? 0 : 1;
}

