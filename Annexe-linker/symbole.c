#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <ctype.h>
#include "symbole.h"
#include "error.h"

/**
 * @file
 * @brief Implantation des symboles
 * @author F. Boulier
 * @date novembre 2010
 */

/**
 * @brief Constructeur. Initialise le symbole S.
 * Cette fonction devrait être appelée avant toute utilisation de S.
 * @param[out] S un symbole
 */

void init_symbole (struct symbole* S)
{
    S->type = '\0';
    S->ident = (char*)0;
    S->nbdef = -1;       /* valeur indéfinie */
}

/**
 * @brief Destructeur.
 * Libère les ressources consommées par S. Cette fonction devrait
 * être appelée après la dernière utilisation de S.
 * @param[in,out] S un symbole
 */

void clear_symbole (struct symbole* S)
{
    if (S->ident)
	free (S->ident);
}

void imprimer_symbole (struct symbole* S)
{
    if (S->ident == (char*)0)
	printf ("<symbole vide>");
    else 
	printf ("<ident = %s, type = %c, nbdef = %d>", 
						S->ident, S->type, S->nbdef);
}

/**
 * @brief Affecte T à S. Toutes les données de T sont dupliquées.
 * @param[out] S un symbole
 * @param[in] T un symbole
 */

void set_symbole (struct symbole* S, struct symbole* T)
{
    if (T->ident)
    {	S->ident = (char*)realloc (S->ident, strlen (T->ident) + 1);
	if (S->ident == NULL)
	    error ("set_symbole", __FILE__, __LINE__);
	strcpy (S->ident, T->ident);
    } else
    {	if (S->ident)
	    free (S->ident);
	S->ident = (char*)0;
    }
    S->type = T->type;
    S->nbdef = T->nbdef;
}

/**
 * @brief Affecte à S le symbole formé de \p type et de \p identi, avec
 * un nombre de définitions \p nbdef.
 * La chaîne \p ident est dupliquée.
 * @param[out] S un symbole
 * @param[in] type un caractère
 * @param[in] ident une chaîne
 * @param[in] nbdef un entier
 */

void set_symbole_tic (struct symbole* S, char type, char* ident, int nbdef)
{
    S->ident = (char*)realloc (S->ident, strlen (ident) + 1);
    if (S->ident == NULL)
	error ("set_symbole_string", __FILE__, __LINE__);
    strcpy (S->ident, ident);
    S->type = type;
    S->nbdef = nbdef;
}

/**
 * Incrémente le nombre de définitions de S.
 * @param[in,out] S un symbole
 */

void ajouter_definition_symbole (struct symbole* S)
{
    S->nbdef += 1;
}

void changer_nbdef_symbole (struct symbole* S, int nbdef)
{
    S->nbdef = nbdef;
}

/**
 * Affecte \p type au champ \p type de S.
 * @param[out] S un symbole
 * @param[in] type un caractère
 */

void changer_type_symbole (struct symbole* S, char type)
{
    S->type = type;
}

/**
 * @brief Retourne \p true si S est un symbole local, \p false sinon.
 * @param[in] S un symbole
 */

bool est_local_symbole (struct symbole* S)
{
    return islower (S->type) && S->type != 'i';
}

/**
 * @brief Retourne \p true si S est un symbole indéfini, \p false sinon
 * @param[in] S un symbole
 */

bool est_indefini_symbole (struct symbole* S)
{
    return S->type == 'U';
}

/**
 * @brief Retourne \p true si S est un symbole défini, \p false sinon
 * @param[in] S un symbole
 */

bool est_defini_symbole (struct symbole* S)
{
    return S->type != 'U';
}

/**
 * @brief Retourne \p true si S est un symbole global défini, \p false sinon
 * @param[in] S un symbole
 */

bool est_global_et_defini_symbole (struct symbole* S)
{
    return (isupper (S->type) && S->type != 'U') || S->type == 'i';
}

/**
 * @brief Retourne \p true si S est un symbole faible, \p false sinon.
 * @param[in] S un symbole
 */

bool est_faible_symbole (struct symbole* S)
{
    return S->type == 'W' || S->type == 'w' || 
					S->type == 'V' || S->type == 'v';
}

/**
 * @brief Retourne \p true si S est dupliqué, \p false sinon.
 * @param[in] S un symbole
 */

bool est_duplique_symbole (struct symbole* S)
{
    return S->nbdef >= 2;
}

