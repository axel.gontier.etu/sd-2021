/* bonne-gestion-memoire.c */
#include <stdio.h>
#include <string.h>
#include <stdlib.h> // pour malloc et free

int main ()
{   char* s;
    s = malloc (16 * sizeof (char)); // de la mémoire pour 16 char
    strcpy (s, "elephant");
    printf ("%s\n", s);
    free (s); // on restitue au tas la mémoire pointée par s
    return 0;
}
