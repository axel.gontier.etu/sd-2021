/* liste.c */

#include <stdio.h>
#include <stdlib.h>
#include "liste.h"

void init_liste (struct liste* L)
{
    L->tete = NIL;
    L->nbelem = 0;
}

void ajout_en_tete_liste (struct liste* L, double v)
{
    struct maillon* M;
    M = (struct maillon*)malloc (sizeof(struct maillon));
    M->valeur = v;
    M->suivant = L->tete;
    L->tete = M;
    L->nbelem += 1;
}

void print_liste (struct liste L)
{
    struct maillon* M;
    M = L.tete;
    for (int i = 0; i < L.nbelem; i++)
    {   printf ("%lf ", M->valeur);
        M = M->suivant;
    }
    printf ("\n");
/*
Variante:
    M = L.tete;
    while (M != NIL)
    {   printf ("%lf ", M->valeur);
        M = M->suivant;
    }
Autre variante:
    for (M = L.tete; M != NIL; M = M->suivant)
        printf ("%lf ", M->valeur);
 */
}

void clear_liste (struct liste* L)
{
/* Ici, on doit faire des free */
}



