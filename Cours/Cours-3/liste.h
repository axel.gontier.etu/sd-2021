/* liste.h */

/* Spécifications : voir le dessin */

/* les maillons = structures "horizontales" dans le tas */

struct maillon {
    double valeur;
    struct maillon* suivant;
};

/* le pointeur nul de type struct maillon* */

#define NIL (struct maillon*)0

/* les listes = structures "verticales" souvent dans la pile */
/* les variables locales seront de type struct liste */

struct liste {
    struct maillon* tete;
    int nbelem;
};

extern void init_liste (struct liste*);
extern void ajout_en_tete_liste (struct liste*, double);
extern void print_liste (struct liste);
extern void clear_liste (struct liste*);
