/* pile.h */

#define DMAX 10

struct pile {
    double T [DMAX];
    int sp;
};

/* 
 * Spécification : 
 * La pile se remplit à partir de l'indice 0.
 * sp contient l'indice de la première case libre (initalisé à 0, donc).
 */

/* Constructeur. Initialise la pile à vide */
extern void init_pile (struct pile*);

extern void clear_pile (struct pile*);

/* Empile d dans P */
extern void empiler (struct pile* P, double d);

/* Dépile un élément de P et l'affecte à d. */
extern void depiler (double* d, struct pile* P);

#include <stdbool.h> // Pour le type bool

/* Retourne true si la pile est vide */
extern bool pile_vide (struct pile*);

