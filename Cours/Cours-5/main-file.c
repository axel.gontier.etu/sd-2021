/* main-file.c */

#include <stdio.h>
#include "file.h"

int main ()
{   double data [] = { 107, 23, -45, 18 };
    int n = sizeof(data)/sizeof(double);

    struct file P;

    init_file (&P);
    for (int i = 0; i < n; i++)
    {   printf ("on enfile : %lf\n", data[i]);
        enfiler (&P, data[i]);
    }
    while (! file_vide (&P))
    {   double d;
        defiler (&d, &P);
        printf ("on vient de defiler %lf\n", d);
    }
    clear_file (&P);
    return 0;
}

