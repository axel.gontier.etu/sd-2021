/* file.h */

#define DMAX 10

struct file {
    double T [DMAX];
    int re;
    int we;
};

/* 
 * Spécification : voir notes de cours
 */

/* Constructeur. Initialise la file à vide */
extern void init_file (struct file*);

extern void clear_file (struct file*);

/* Enfile d dans P */
extern void enfiler (struct file* P, double d);

/* Défile un élément de P et l'affecte à d. */
extern void defiler (double* d, struct file* P);

#include <stdbool.h> // Pour le type bool

/* Retourne true si la file est vide */
extern bool file_vide (struct file*);

