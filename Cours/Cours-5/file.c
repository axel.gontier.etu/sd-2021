/* file.c */

#include <assert.h>
#include "file.h"

void init_file (struct file* F)
{
    F->re = 0;
    F->we = 1;
}

void clear_file (struct file* F)
{
}

/* Enfile d dans F */
void enfiler (struct file* F, double d)
{
    F->we = (F->we - 1 + DMAX) % DMAX;
    F->T [F->we] = d;
}
    
/* Défile un élément de F et l'affecte à d. */
void defiler (double* d, struct file* F)
{
    assert (! file_vide (F));
    *d = F->T [F->re];
    F->re = (F->re - 1 + DMAX) % DMAX;
}

/* Retourne true si la file est vide */
bool file_vide (struct file* F)
{
    return (F->we - 1 + DMAX) % DMAX == F->re;
}

