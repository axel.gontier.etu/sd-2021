/* main.c */

#include <stdio.h>
#include "pile.h"

int main ()
{   double data [] = { 107, 23, -45, 18 };
    int n = sizeof(data)/sizeof(double);

    struct pile P;

    init_pile (&P);
    for (int i = 0; i < n; i++)
    {   printf ("on empile : %lf\n", data[i]);
        empiler (&P, data[i]);
    }
    while (! pile_vide (&P))
    {   double d;
        depiler (&d, &P);
        printf ("on vient de depiler %lf\n", d);
    }
    clear_pile (&P);
    return 0;
}

