
/* point.c */

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <point.h>

int compare_doubles (const void* a0, const void* b0)

{   double* a = (double*)a0; // crois-moi : a0 est un double*

    double* b = (double*)b0;



    if (*a < *b)

        return 1;

    else if (*a == *b)

        return 0;

    else

        return -1;

}

int compare_points (const void* a0, const void* b0)
{
struct point* a=(struct point*)a0;
struct point* b=(struct point*)b0;
double xu= a->x;
double yu= a->y;
double xv= b->x;
double yv= b->y;
double det = xu*yv-yu*xv;
if(det>0)
return -1;
else
return 1;

}

bool tourne_a_gauche (struct point* ao, struct point* bo, struct point* co)
{
struct point *A=(struct point*)ao;
struct point *B=(struct point*)bo;
struct point *C=(struct point*)ao;
double xa=A->x,ya=A->y,xb=B->x,yb=B->y,xc=C->x,yc=C->y;
double signe=(xb-xa)*(yc-ya)-(yb-ya)*(xc-xa);
if(signe>0)//si l'angle est positif on tourne a gauche
return true;
else
return false;
}

void init_point (struct point* p, double x, double y, char nom)
{p->x=x;
p->y=y;;
p->ident=nom;
}




