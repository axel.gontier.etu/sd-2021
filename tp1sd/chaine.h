typedef struct {
	 int *c;
	 int n;
	 int j;
}chaine;
/*Specification du type
 *le type struct chaine implante une chaine de caractère
 le champ c contient un caractère
 le champ n correspond a la taille initiale choisie de la chaine
 le champ j correspond au premier caractère non défini
*/


void constructeur(chaine *ch);

void destructeur(chaine *ch);

void printer(chaine ch);

void add(chaine *c,char car);

