//liste_char.c
#include <stdio.h>
#include <stdlib.h>
#include "liste_char.h"



void init_liste(struct liste* L)
{

L->tete=NIL;
L->nbelem=0;
}

void ajout_en_tete_liste(struct liste* L,char c)
{
struct maillon* M;
M=(struct maillon*)malloc(sizeof(struct maillon));
M->valeur=c;
M->suivant=L->tete;
L->tete=M;//maillon ou l'on pointe
L->nbelem+=1;
}

void print_liste(struct liste L)
{
struct maillon *M;
M=L.tete;
while(M!=NIL)
{
printf("%c",M->valeur);
M=M->suivant;//passage au maillon suivant
}
printf("\n");
}

void clear_liste(struct liste* L)
{
struct maillon *M,*tmp;
M=L->tete;
while(M!=NIL)
{tmp=M->suivant;
free(M);
M=tmp;
}
}










