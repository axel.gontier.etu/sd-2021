//liste_char.h
struct maillon{
	char valeur;
	struct maillon* suivant;
};
struct liste{
struct maillon* tete;
int nbelem;
};

#define NIL (struct maillon*)0
extern void init_liste (struct liste*);
extern void ajout_en_tete_liste (struct liste*, char);
extern void print_liste (struct liste);
extern void clear_liste (struct liste*);

