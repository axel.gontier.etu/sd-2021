#include "liste_char.h"
typedef struct {
	 struct liste L;
}chaine;
/*Specification du type
 *le type struct chaine implante une chaine de caractère
 le champ L contient une chaine de caractère sous forme de liste chainée.
*/


void constructeur(chaine *ch);

void destructeur(chaine *ch);

void print(chaine ch);

void add(chaine *c,char car);

